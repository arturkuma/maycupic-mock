import {
	makeExecutableSchema,
	addMockFunctionsToSchema, MockList,
} from 'graphql-tools';

import { ApolloServer, gql } from 'apollo-server';

import * as faker from 'faker';

import interfaces from './graphql/interfaces';
import types from './graphql/types';
import enums from './graphql/enums';
import query from "./graphql/query";
import inputs from "./graphql/input";
import mutations from "./graphql/mutations";

const typeDefs = [
	// interfaces,
	types,
	enums,
	query,
	inputs,
	mutations
];

export const schema = makeExecutableSchema({ typeDefs });

const mocks = {
	Token: () => ({
		token: faker.random.uuid()
	}),
    File: () => ({
		src: faker.image.imageUrl()
    }),
	User: () => ({
		username: faker.internet.userName(),
        email: faker.internet.email(),
        postsCount: faker.random.number(),
        followersCount: faker.random.number(),
        followingCount: faker.random.number(),
        isFollowedByCurrentUser: faker.random.boolean()
	}),
	Comment: () => ({
        content: faker.lorem.words(),
        createdAt: faker.date.past()
	}),
	Post: () => ({
        description: faker.lorem.words(),
        createdAt: faker.date.past(),
        likesCount: faker.random.number(),
        lastComments: (root, {limit = 5}) => new MockList(limit),
        isLikedByCurrentUser: faker.random.boolean()
	}),
	Tag: () => ({
        name: faker.lorem.word(),
        postsCount: faker.random.number()
	}),
    Company: () => ({
		name: faker.random.arrayElement(['Loreal', 'Isana', 'GetTheLondonLook'])
	}),
	Message: () => ({
        sendAt: faker.date.past(),
        hasBeenReadByCurrentUser: faker.random.boolean(),
        content: faker.lorem.words()
	}),
    Conversation: () => ({
        unreadCount: faker.random.number({
			min: 0,
			max: 10
		})
	}),
	Query: () => ({
        getPosts: (root, {limit = 10}) => new MockList(limit),
        getFollowers: (root, {limit = 10}) => new MockList(limit),
        getFollowing: (root, {limit = 10}) => new MockList(limit),
        getFollowedUsersPosts: (root, {limit = 10}) => new MockList(limit),
        getComments: (root, {limit = 10}) => new MockList(limit),
        searchPosts: (root, {limit = 10}) => new MockList(limit),
        searchUsers: (root, {limit = 10}) => new MockList(limit),
        searchTags: (root, {limit = 10}) => new MockList(limit),
        getConversations: (root, {limit = 10}) => new MockList(limit),
        getPostsForVotting: (root, {limit = 10}) => new MockList(limit),
	})
};

addMockFunctionsToSchema({ schema, mocks });

const server = new ApolloServer({ typeDefs, schema });

server.listen().then(({ url }) => {
	console.log(`🚀  Server ready at ${url}`);
});
