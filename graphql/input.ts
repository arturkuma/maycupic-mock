import { gql } from 'apollo-server';

export default gql`

  input Pagination {
  	page: Int
  	limit: Int
  }

  input PasswordInput {
    old: String!,
    new: String!
  }

  input CompanyInput {
    name: String!
  }

  input UserInput {
    coverPhotoId: String,
    description: String,
    city: String,
    type: UserType,
    gender: Gender,
    favCompanies: [CompanyInput],
    password: PasswordInput
  }
    
  input MessageInput {
    message: String!
  }
    
  input PostInput {
    fileId: String!,
    description: String,
    submitToCompetition: Boolean
  }  
    
`;
