import { gql } from 'apollo-server';

export default gql`

type Query {
    getPost(postId: String): Post
    getPosts(userId: String!, limit: Int, offset: Int): [Post]
    
    getFollowers(userId: String!, limit: Int, offset: Int): [User]
    getFollowing(userId: String!, limit: Int, offset: Int): [User]
    
    getFollowedUsersPosts(limit: Int, offset: Int): [Post]
    
    getComments(postId: String!, limit: Int, offset: Int): [Comment]
    
    searchPosts(search: String, limit: Int, offset: Int): [Post]
    searchUsers(search: String, limit: Int, offset: Int): [User]
    searchTags(search: String, limit: Int, offset: Int): [Tag]
    
    getConversations(limit: Int, offset: Int): [Conversation]
    
    userHasAnyPendingNotificationOrMessage: Boolean
    
    getPostsForVotting(limit: Int): [Post]
}

`;
