import { gql } from 'apollo-server';

export default gql`
  
  type Token {
  	token: String!,
  	user: User!
  }
  
  type User {
    coverPhoto: File,
    username: String!,
    email: String!,
    postsCount: Int!,
    followersCount: Int!,
    followingCount: Int!,
    isFollowedByCurrentUser: Boolean!
    favCompanies: [Company]
  }

  type File {
    src: String!
  }

  type Comment {
    user: User!,
    content: String!,
    createdAt: String
  }

  type Post {
    file: File!,
    description: String,
    createdAt: String!,
    user: User,
    likesCount: Int!,
    lastComments(limit: Int): [Comment],
    isLikedByCurrentUser: Boolean!
  }
  
  type Tag {
    name: String!,
    postsCount: Int!
  }
  
  type Company {
    name: String!
  }
  
  type Message {
    from: User!,
    sendAt: String!,
    content: String,
    hasBeenReadByCurrentUser: Boolean!
  }
  
  type Conversation {
    user: User!,
    lastMessage: Message!,
    unreadCount: Int!
  }
  
`;
