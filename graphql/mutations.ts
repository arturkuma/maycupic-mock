import { gql } from 'apollo-server';

export default gql`

type Mutation {
  	authenticate(usernameOrEmail: String!, password: String!): Token
  	logout: Boolean
  	resetPassword(usernameOrEmail: String!): Boolean
  	
  	likePost(postId: String!): Boolean
  	
  	updateProfile(userId: String!, input: UserInput!): Boolean
  	
  	sendMessage(userId: String!, input: MessageInput!): Message
  	markMessageAsRead(messageId: String!): Boolean
  	
  	addPost(input: PostInput): Post
  	editPost(postId: String!, input: PostInput): Post
  	
  	voteForPost(postId: String!, positive: Boolean!): Boolean
}

`;
